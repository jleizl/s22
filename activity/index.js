/*
    Create functions which can manipulate our arrays.
    You may change the names on the initial list before you use the array varaibles.
*/

let registeredUsers = [

    "Gracelle Ferro",
    "Marshal Grace Amparo",
    "Kena Caguicla",
    "Mafe Abante",
    "Malyn Gonzales",
    "Myra de Luna",
    "Raquilyn Padolina"
];

let friendsList = [];


/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.

*/

function registerFriend(friendName) {

    let doesUserRegistered = registeredUsers.includes(friendName);

    if (doesUserRegistered) {
        alert("Registration failed. " + friendName + " already exists!")
    } else {
        registeredUsers.push(friendName);
        alert("Thank you for registering!")

    }
}

console.log("Register a friend:");

registerFriend("Gracelle Ferro");
console.log(registeredUsers);

registerFriend("Lyanne Tañada");
console.log(registeredUsers);


/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/


function getFriendsList(name) {

    let foundUser = registeredUsers.includes(name);

    if (foundUser) {
        friendsList.push(name);
        alert("You have added " + name + " as a friend!")
    } else {
        alert("User not found.");
    }
}

console.log("Add registered user into friends list: ")

getFriendsList("Epe Amparo");
console.log(friendsList);
getFriendsList("Rhoane Tañada");
console.log(friendsList);
getFriendsList("Marshal Grace Amparo");
console.log(friendsList);
getFriendsList("Lyanne Tañada");
console.log(friendsList);
getFriendsList("Kena Caguicla");
console.log(friendsList);
getFriendsList("Mafe Abante");
console.log(friendsList);
/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.

*/

function isFriendListEmpty() {

    let showfriendsList = friendsList.map(function(friend) {
        console.log(friend)
    })
}
console.log(friendsList)
console.log("Display friendsList one by one: ")
isFriendListEmpty();



// STRETCH GOALS

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.
*/

function displayRegisteredUsers() {

    let amountOfFriends = friendsList.length;
    if (amountOfFriends == 0) {
        alert("You currently have 0 friends. Add one first.")
    } else {
        alert("You currently have " + amountOfFriends + " friends.")
    }

}

displayRegisteredUsers();


/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - In the browser console, log the friendsList array.

*/

function deleteLastRegisteredUser() {

    let registeredFriends = friendsList.length;
    if (registeredFriends == 0) {
        alert("You currently have 0 friends. Add one first.")
    } else {
        friendsList.pop()
    }
}

deleteLastRegisteredUser();
console.log(friendsList);

/*======================================================================================*/



// Try this for fun:



// Instead of only deleting the last registered user in the friendsList delete a specific user instead.
// -You may get the user's index.
// -Then delete the specific user with splice().


let friendsListSplice = friendsList.splice(1)
console.log(friendsList);
console.log(friendsListSplice)